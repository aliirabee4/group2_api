import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:login_g2/screens/subscribtion/model.dart';

class CompetitionSubscribtionController {
  Dio _dio = Dio();
  CompetitionSubscribtionModel _competitionSubscribtionModel =
      CompetitionSubscribtionModel();
  Future<CompetitionSubscribtionModel> subscribe(
      {int id,
      String name,
      String email,
      String phone,
      List<Map<String, dynamic>> answers}) async {
    _dio.options.baseUrl = 'https://association.rowadtqnee.sa/api';
    FormData _formData = FormData.fromMap(
        {'name': name, 'phone': phone, 'email': email, 'answers': answers});
    var response =
        await _dio.post('/subscription_competition/$id', data: _formData);
    print(response);
    var data = json.decode(response.toString());
    _competitionSubscribtionModel = CompetitionSubscribtionModel.fromJson(data);
    return _competitionSubscribtionModel;
  }
}
