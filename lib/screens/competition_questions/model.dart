class CometitionQuestionsModel {
  int status;
  Data data;

  CometitionQuestionsModel({this.status, this.data});

  CometitionQuestionsModel.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.data != null) {
      data['data'] = this.data.toJson();
    }
    return data;
  }
}

class Data {
  int competitionId;
  String competitionTitle;
  List<Questions> questions;

  Data({this.competitionId, this.competitionTitle, this.questions});

  Data.fromJson(Map<String, dynamic> json) {
    competitionId = json['competition_id'];
    competitionTitle = json['competition_title'];
    if (json['questions'] != null) {
      questions = new List<Questions>();
      json['questions'].forEach((v) {
        questions.add(new Questions.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['competition_id'] = this.competitionId;
    data['competition_title'] = this.competitionTitle;
    if (this.questions != null) {
      data['questions'] = this.questions.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Questions {
  int id;
  int competitionId;
  String question;
  String answer;
  String answerOne;
  String answerTwo;
  String answerThree;
  String answerFour;

  Questions(
      {this.id,
      this.competitionId,
      this.question,
      this.answer,
      this.answerOne,
      this.answerTwo,
      this.answerThree,
      this.answerFour});

  Questions.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    competitionId = json['competition_id'];
    question = json['question'];
    answer = json['answer'];
    answerOne = json['answer_one'];
    answerTwo = json['answer_two'];
    answerThree = json['answer_three'];
    answerFour = json['answer_four'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['competition_id'] = this.competitionId;
    data['question'] = this.question;
    data['answer'] = this.answer;
    data['answer_one'] = this.answerOne;
    data['answer_two'] = this.answerTwo;
    data['answer_three'] = this.answerThree;
    data['answer_four'] = this.answerFour;
    return data;
  }
}
