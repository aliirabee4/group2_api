import 'package:flutter/material.dart';
import 'package:login_g2/screens/competition/appbar.dart';
import 'package:login_g2/screens/competition_questions/controller.dart';
import 'package:login_g2/screens/competition_questions/model.dart';
import 'package:login_g2/screens/subscribtion/controller.dart';
import 'package:login_g2/screens/subscribtion/model.dart';

class CompetitionQuestionsView extends StatefulWidget {
  @override
  _CompetitionQuestionsViewState createState() =>
      _CompetitionQuestionsViewState();
}

class _CompetitionQuestionsViewState extends State<CompetitionQuestionsView> {
  CometitionQuestionsModel _cometitionQuestionsModel =
      CometitionQuestionsModel();
  CompetitionQuestionsController _competitionQuestionsController =
      CompetitionQuestionsController();
  bool _loading = true;

  int _currentQuestion = 0;

  PageController _pageController = PageController();

  CompetitionSubscribtionModel _competitionSubscribtionModel =
      CompetitionSubscribtionModel();
  CompetitionSubscribtionController _competitionSubscribtionController =
      CompetitionSubscribtionController();

  bool _subscribeLoading = false;

  List<Map<String, dynamic>> _answers = [
    {"id": 35, "answer": "أيوة مفيش الكلام ده"},
    {"id": 36, "answer": "انت إنسان مش محترم"}
  ];

  @override
  void initState() {
    _getData();
    super.initState();
  }

  _subscribe() async {
    setState(() {
      _subscribeLoading = true;
    });
    _competitionSubscribtionModel =
        await _competitionSubscribtionController.subscribe(
            name: 'Nacncsr',
            id: 12,
            email: 'Aljhhhhi@a5dr.com',
            phone: '12345689966445',
            answers: _answers);
    setState(() {
      _subscribeLoading = false;
    });
  }

  _getData() async {
    _cometitionQuestionsModel =
        await _competitionQuestionsController.getCompetitionQuestions();
    setState(() {
      _cometitionQuestionsModel.data.questions.add(Questions(
          id: 36,
          competitionId: 12,
          question: "سيسصير سي صسشي سي ص",
          answer: "text",
          answerOne: "",
          answerTwo: "",
          answerThree: "",
          answerFour: ""));
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
      textDirection: TextDirection.rtl,
      child: Scaffold(
          appBar: customAppBar(context: context, title: 'المسابقة'),
          body: _loading
              ? Center(
                  child: CircularProgressIndicator(),
                )
              : Column(
                  children: [
                    Expanded(
                      child: PageView.builder(
                          controller: _pageController,
                          itemCount:
                              _cometitionQuestionsModel.data.questions.length,
                          itemBuilder: (ctx, index) {
                            return (() {
                              if (_cometitionQuestionsModel
                                      .data.questions[index].answer ==
                                  'image') {
                                return _imageQuestion(
                                    image: _cometitionQuestionsModel
                                        .data.questions[index].question);
                              } else if (_cometitionQuestionsModel
                                      .data.questions[index].answer ==
                                  'choose') {
                                return _chooseQuestion(
                                    answer1: _cometitionQuestionsModel
                                        .data.questions[index].answerOne,
                                    answer2: _cometitionQuestionsModel
                                        .data.questions[index].answerTwo,
                                    answer3: _cometitionQuestionsModel
                                        .data.questions[index].answerThree,
                                    answer4: _cometitionQuestionsModel
                                        .data.questions[index].answerFour,
                                    question: _cometitionQuestionsModel
                                        .data.questions[index].question);
                              } else {
                                return _textQuestion(
                                    question: _cometitionQuestionsModel
                                        .data.questions[index].question);
                              }
                            }());
                          }),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          _currentQuestion == 0
                              ? Container()
                              : InkWell(
                                  onTap: () {
                                    setState(() {
                                      _currentQuestion--;
                                    });
                                    _pageController
                                        .jumpToPage(_currentQuestion);
                                  },
                                  child: Container(
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.teal),
                                    child: Icon(
                                      Icons.arrow_back_ios,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                          _currentQuestion <
                                  _cometitionQuestionsModel
                                          .data.questions.length -
                                      1
                              ? InkWell(
                                  onTap: () {
                                    setState(() {
                                      _currentQuestion++;
                                    });
                                    _pageController
                                        .jumpToPage(_currentQuestion);
                                  },
                                  child: Container(
                                    width: 50,
                                    height: 50,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(5),
                                        color: Colors.teal),
                                    child: Icon(
                                      Icons.arrow_forward_ios,
                                      color: Colors.white,
                                    ),
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                    ),
                    SizedBox(
                      height: 50,
                    ),
                    _currentQuestion ==
                            _cometitionQuestionsModel.data.questions.length - 1
                        ? InkWell(
                            onTap: () {
                              _subscribe();
                            },
                            child: Container(
                              margin: EdgeInsets.symmetric(
                                  horizontal: 20, vertical: 20),
                              width: MediaQuery.of(context).size.width,
                              height: 50,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.teal),
                              child: Center(
                                child: _subscribeLoading
                                    ? CircularProgressIndicator()
                                    : Text('ارسال'),
                              ),
                            ),
                          )
                        : Container(),
                    SizedBox(
                      height: 40,
                    )
                  ],
                )),
    );
  }

  Widget _textQuestion({String question}) {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: 200,
          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Text(question),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
          child: TextFormField(
            maxLines: 4,
            decoration: InputDecoration(
                hintText: 'اكتب اجابتك هنا',
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10))),
          ),
        )
      ],
    );
  }

  Widget _imageQuestion({String image}) {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: 200,
          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              image: DecorationImage(
                  image: NetworkImage(image), fit: BoxFit.cover)),
        ),
        Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
          child: TextFormField(
            maxLines: 4,
            decoration: InputDecoration(
                hintText: 'اكتب اجابتك هنا',
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(10))),
          ),
        )
      ],
    );
  }

  Widget _chooseQuestion({
    String question,
    String answer1,
    String answer2,
    String answer3,
    String answer4,
  }) {
    return Column(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: 200,
          margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
          ),
          child: Text(question),
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _selectCard(choose: answer1),
            _selectCard(choose: answer2),
          ],
        ),
        SizedBox(
          height: 10,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            _selectCard(choose: answer3),
            _selectCard(choose: answer4),
          ],
        ),
      ],
    );
  }

  Widget _selectCard({String choose}) {
    return Container(
      width: MediaQuery.of(context).size.width / 2 - 20,
      height: 50,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(5),
        color: Colors.grey[300],
      ),
      child: Center(
        child: Text(choose),
      ),
    );
  }
}
