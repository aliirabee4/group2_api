import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:login_g2/screens/competition_questions/model.dart';

class CompetitionQuestionsController {
  Dio _dio = Dio();
  CometitionQuestionsModel _cometitionQuestionsModel =
      CometitionQuestionsModel();
  Future<CometitionQuestionsModel> getCompetitionQuestions() async {
    _dio.options.baseUrl = 'https://association.rowadtqnee.sa/api';
    var response = await _dio.get('/competition/questions');
    var data = json.decode(response.toString());
    _cometitionQuestionsModel = CometitionQuestionsModel.fromJson(data);
    return _cometitionQuestionsModel;
  }
}
