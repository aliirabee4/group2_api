import 'package:flutter/material.dart';
import 'package:login_g2/screens/competition/textfield.dart';
import 'package:login_g2/screens/competition_questions/view.dart';
import 'appbar.dart';

class CompetitionView extends StatefulWidget {
  @override
  _CompetitionViewState createState() => _CompetitionViewState();
}

class _CompetitionViewState extends State<CompetitionView> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();

  void _submitForm() async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => CompetitionQuestionsView()));
  }

  Widget _view() {
    return Expanded(
        child: ListView.builder(
            itemCount: 2,
            itemBuilder: (ctx, index) {
              return Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 8),
                child: Row(
                  children: [
                    Container(
                      width: 10,
                      height: 10,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          color: Colors.grey),
                    ),
                    SizedBox(
                      width: 6,
                    ),
                    Text('desc')
                  ],
                ),
              );
            }));
  }

  @override
  Widget build(BuildContext context) {
    return Directionality(
        textDirection: TextDirection.rtl,
        child: Scaffold(
          key: _globalKey,
          backgroundColor: Theme.of(context).backgroundColor,
          appBar: customAppBar(context: context, title: 'المسابقة'),
          body: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                _title(title: 'للاشتراك ادخل البيانات التالية'),
                textField(
                    hint: 'الاسم الرباعي',
                    type: TextInputType.text,
                    onSave: (value) {
                      setState(() {});
                    },
                    validate: (value) {
                      if (value.toString().isEmpty) {
                        return 'الاسم مطلوب';
                      } else {
                        return null;
                      }
                    }),
                textField(
                    hint: 'رقم الجوال',
                    type: TextInputType.number,
                    onSave: (value) {
                      setState(() {});
                    },
                    validate: (value) {
                      if (value.toString().isEmpty ||
                          value.toString().length < 10) {
                        return 'رقم الجوال مطلوب';
                      } else {
                        return null;
                      }
                    }),
                textField(
                    hint: 'البريد الالكتروني',
                    type: TextInputType.emailAddress,
                    onSave: (value) {
                      setState(() {});
                    },
                    validate: (value) {
                      if (value.toString().isEmpty ||
                          !RegExp(r"[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
                              .hasMatch(value)) {
                        return 'البريد الالكتروني مطلوب';
                      } else {
                        return null;
                      }
                    }),
                _title(title: 'شروط المسابقة'),
                _view()
              ],
            ),
          ),
          bottomNavigationBar: InkWell(
            onTap: () {
              _submitForm();
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(5),
                  color: Theme.of(context).accentColor),
              child: Center(
                child: Text(
                  'اشترك الأن',
                  style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 18),
                ),
              ),
            ),
          ),
        ));
  }

  Widget _title({String title}) {
    return Padding(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
      child: Row(
        children: <Widget>[
          Text(
            title,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 16),
          )
        ],
      ),
    );
  }
}
