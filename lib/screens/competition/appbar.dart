import 'package:flutter/material.dart';

Widget customAppBar({BuildContext context, String title}) {
  return AppBar(
    leading: InkWell(
      onTap: () {
        Navigator.of(context).pop();
      },
      child: Container(
        width: 15,
        height: 15,
        margin: EdgeInsets.only(top: 12, bottom: 12, right: 16),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            color: Theme.of(context).primaryColor),
        child: Center(
          child: Icon(
            Icons.arrow_back_ios,
            size: 20,
            color: Color.fromRGBO(211, 173, 106, 1),
          ),
        ),
      ),
    ),
    elevation: 0,
    backgroundColor: Theme.of(context).backgroundColor,
    centerTitle: false,
    automaticallyImplyLeading: false,
    title: Text(
      title,
      style: TextStyle(
          color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
    ),
  );
}
