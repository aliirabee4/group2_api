import 'package:flutter/material.dart';

Widget textField(
    {String hint,
    Function validate,
    Function onSave,
    TextInputType type,
    TextEditingController controller,
    int lineNumber = 1}) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 10),
    child: TextFormField(
      controller: controller,
      keyboardType: type,
      maxLines: lineNumber,
      validator: validate,
      onSaved: onSave,
      decoration: InputDecoration(
          hintText: hint,
          filled: true,
          fillColor: Colors.white,
          contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 6),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(10),
              borderSide: BorderSide.none)),
    ),
  );
}
