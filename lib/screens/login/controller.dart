import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:login_g2/screens/login/model.dart';

class LoginController {
  LoginModel _loginModel = LoginModel();

  Future<LoginModel> userLogin(
      {String password,
      String phone,
      String deviceType,
      String deviceId,
      String fcmToken}) async {
    Dio _dio = Dio();
    FormData _formData = FormData.fromMap({
      'phone': phone,
      'password': password,
      'type': deviceType,
      'device_id': deviceId,
      'fcm_token': fcmToken
    });
    var response = await _dio
        .post('https://tawlah.rowadtqnee.sa/api/auth/login', data: _formData);

    if (response.data['status'] == 200) {
      Map<String, dynamic> data = json.decode(response.toString());
      _loginModel = LoginModel.fromJson(data);
    } else {
      _loginModel = null;
    }
    return _loginModel;
  }
}
