import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:login_g2/screens/login/controller.dart';
import 'package:login_g2/screens/login/model.dart';
import 'package:toast/toast.dart';

class LoginView extends StatefulWidget {
  @override
  _LoginViewState createState() => _LoginViewState();
}

class _LoginViewState extends State<LoginView> {
  String _phoneNumber;
  String _password;

  LoginController _loginController = LoginController();
  LoginModel _loginModel = LoginModel();

  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  String _fcmToken;

  GlobalKey<FormState> _globalKey = GlobalKey<FormState>();
  bool _loading = false;

  @override
  void initState() {
    _firebaseMessaging.getToken().then((value) {
      _fcmToken = value;
    });
    super.initState();
  }

  _userLogin() async {
    setState(() {
      _loading = true;
    });
    _loginModel = await _loginController.userLogin(
        password: _password,
        phone: _phoneNumber,
        deviceId: 'dfghjk',
        deviceType: 'ios',
        fcmToken: _fcmToken);
    setState(() {
      _loading = false;
    });
    if (_loginModel == null) {
      Toast.show('Incorrect Data', context);
    } else {}
  }

  Widget _textField(
      {String hint,
      bool secure,
      TextInputType type,
      Function validate,
      Function onSave}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: TextFormField(
        validator: validate,
        obscureText: secure,
        keyboardType: type,
        onSaved: onSave,
        decoration: InputDecoration(
            hintText: hint,
            contentPadding: EdgeInsets.symmetric(horizontal: 10),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(20),
            )),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: ListView(
        children: [
          SizedBox(
            height: 120,
          ),
          Form(
              key: _globalKey,
              child: Column(
                children: [
                  _textField(
                    onSave: (value) {
                      setState(() {
                        _phoneNumber = value;
                      });
                    },
                    validate: (value) {
                      if (value.toString().isEmpty ||
                          value.toString().length < 6) {
                        return 'Phone Requerd and must be more than 6';
                      } else {
                        return null;
                      }
                    },
                    hint: 'Mobile Number',
                    secure: false,
                    type: TextInputType.number,
                  ),
                  _textField(
                    onSave: (value) {
                      setState(() {
                        _password = value;
                      });
                    },
                    validate: (value) {
                      if (value.toString().isEmpty) {
                        return 'Password Requerd';
                      } else {
                        return null;
                      }
                    },
                    hint: 'Password',
                    secure: true,
                    type: TextInputType.text,
                  ),
                ],
              )),
          InkWell(
            onTap: () {
              if (!_globalKey.currentState.validate()) {
                return;
              } else {
                _globalKey.currentState.save();
                _userLogin();
              }
            },
            child: _loading
                ? Center(child: Text('Loading'))
                : Container(
                    width: MediaQuery.of(context).size.width,
                    height: 50,
                    margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20),
                        color: Colors.teal),
                    child: Center(
                      child: Text(
                        'Login',
                        style: TextStyle(color: Colors.white, fontSize: 20),
                      ),
                    ),
                  ),
          )
        ],
      ),
    );
  }
}
