import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:login_g2/screens/home/model.dart';

class HomeController {
  Dio dio = Dio();

  HomeModel _homeModel = HomeModel();

  Future<HomeModel> getRestaurants() async {
    dio.options.baseUrl = 'https://tawlah.rowadtqnee.sa/api';
    var response = await dio.get('/home');
    Map<String, dynamic> _data = json.decode(response.toString());
    _homeModel = HomeModel.fromJson(_data);
    return _homeModel;
  }
}
