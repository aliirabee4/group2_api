import 'package:flutter/material.dart';
import 'package:login_g2/screens/home/model.dart';
import 'package:login_g2/screens/home/place_card.dart';

class PlacesList extends StatelessWidget {
  final HomeModel homeModel;

  const PlacesList({Key key, this.homeModel}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      itemCount: homeModel.data.restaurants.length,
      itemBuilder: (BuildContext context, int index) {
        return PlaceCard(
          image: homeModel.data.restaurants[index].mainImage,
          name: homeModel.data.restaurants[index].name,
          cat: homeModel.data.restaurants[index].category,
          address: homeModel.data.restaurants[index].address,
        );
      },
    );
  }
}
