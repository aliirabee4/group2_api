import 'package:flutter/material.dart';
import 'package:login_g2/homeAppBar.dart';
import 'package:login_g2/screens/home/controller.dart';
import 'package:login_g2/screens/home/model.dart';
import 'package:login_g2/screens/home/places_list.dart';
import '../../textField.dart';

class HomeView extends StatefulWidget {
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<HomeView> {
  HomeController _homeController = HomeController();
  HomeModel _homeModel = HomeModel();
  bool _loading = true;

  @override
  initState() {
    _getRestaurants();
    super.initState();
  }

  _getRestaurants() async {
    _homeModel = await _homeController.getRestaurants();
    setState(() {
      _loading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: homeAppBar(context: context),
      body: Column(
        children: [
          textField(),
          _loading
              ? CircularProgressIndicator()
              : Expanded(
                  child: PlacesList(
                  homeModel: _homeModel,
                ))
        ],
      ),
    );
  }
}
